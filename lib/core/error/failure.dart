import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final String errorCode;

  const Failure({this.errorCode = ''});

  @override
  List<Object> get props => [errorCode];

  @override
  String toString() => errorCode;
}

// General failures
class ServerFailure extends Failure {

  const ServerFailure({errorCode = ''}) : super(errorCode: errorCode);
}

class CacheFailure extends Failure {}
