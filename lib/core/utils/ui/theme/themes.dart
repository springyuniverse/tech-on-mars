import 'package:flutter/material.dart';
import 'package:techonmars/core/utils/ui/theme/app_theme.dart';

abstract class AppTheme {
  static ThemeData get lightTheme => ThemeData(
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(primary: const Color(0xff2E66D4))),
        scaffoldBackgroundColor: AppColors.background,
        brightness: Brightness.light,
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: Colors.black, foregroundColor: Colors.white),
        appBarTheme: const AppBarTheme(
            color: AppColors.secondary, foregroundColor: Colors.black),
        textTheme: ThemeData.dark().textTheme.copyWith(
              titleSmall: const TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto'),
              bodyLarge: const TextStyle(
                  fontSize: 16,
                  color: Color(0xff909094),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto'),
              bodyMedium: const TextStyle(
                  fontSize: 14,
                  color: Color(0xff909094),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto'),
            ),
      );

  static ThemeData get darkTheme => ThemeData(
        brightness: Brightness.dark,
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(primary: const Color(0xff2E66D4))),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: Colors.white, foregroundColor: Colors.black),
        scaffoldBackgroundColor: Colors.black87,

        appBarTheme:
            const AppBarTheme(color: Color(0xff303134), foregroundColor: Colors.white),
        textTheme: ThemeData.dark().textTheme.copyWith(
              titleSmall: const TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto'),
              bodyLarge: const TextStyle(
                  fontSize: 16,
                  color: Color(0xff909094),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto'),
              bodyMedium: const TextStyle(
                  fontSize: 14,
                  color: Color(0xff909094),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto'),
            ),
      );
}
