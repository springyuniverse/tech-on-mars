import 'package:flutter/material.dart';

///
/// Providing the app's theme colors
/// [PRIMARY] Primary color: white
/// [ACCENT] Accent color: green
/// [ACCENT_DARK] Dark accent color: dark green
/// [TEXT] Text color: green
///
class AppColors {
  /// Main app colors

  static const Color secondary = Colors.white;
  static const Color background = Color(0xffF9F9F9);
}
