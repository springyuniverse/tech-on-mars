import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainAppBar extends AppBar {
  MainAppBar(
      {Key? key,
      Widget? title,
      bool isCentered = true,
      required BuildContext context})
      : super(
          key: key,
          title: title,
          centerTitle: isCentered,
          elevation: 0.0,
          systemOverlayStyle: SystemUiOverlayStyle(

            // Status bar color
            statusBarColor: Theme.of(context).brightness == Brightness.light
                ? Colors.white
                : const Color(0xff303134),

            statusBarIconBrightness:
            Theme.of(context).brightness == Brightness.light
                ? Brightness.dark
                : Brightness.light, // For Android (dark icons)
            statusBarBrightness:         Theme.of(context).brightness == Brightness.light
                ? Brightness.dark
                : Brightness.light, // For iOS (dark icons)
          ),
        );
}
