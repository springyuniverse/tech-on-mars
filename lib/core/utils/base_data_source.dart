import 'package:dio/dio.dart';
import 'models_mapper.dart';

class BaseDataSource<T> {
  final dio = Dio();

  Future<List<T>> getListOfData(String path) async {
    final response = await dio.get(path);

    return ((response.data) as List).map<T>((e) => models[T](e) as T).toList();
  }
}
