import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SizeConfig {
  static final SizeConfig instance = SizeConfig._init();
  SizeConfig._init();

  late double? screenWidth;
  late double? screenHeight;
  late double? defaultSize;

  double getContainerWidth() =>
      screenWidth! > 800 ? screenWidth! * 0.55 : screenWidth!;
  double getCardHeight() =>
      screenWidth! > 800 ? screenHeight! * 0.55 : screenHeight! * 0.40;

  factory SizeConfig(BuildContext context) {
    MediaQueryData _mediaQueryData = MediaQuery.of(context);
    instance.screenWidth = _mediaQueryData.size.width;
    instance.screenHeight = _mediaQueryData.size.height;



    return instance;
  }
}
