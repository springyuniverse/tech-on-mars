import 'package:techonmars/modules/main/data/models/generic_content_model.dart';

/// models should be registered here
final Map models = {
  GenericContentModel: (data) => GenericContentModel.fromJson(data)
};
