import 'package:flutter/cupertino.dart';
import 'package:techonmars/modules/main/presentation/components/article_detail_screen.dart';
import 'package:techonmars/modules/main/presentation/components/image_detail_screen.dart';

import '../../../modules/main/presentation/components/home_screen.dart';

enum Direction { none, top, right, bottom, left }

class AppRouter {
  static const main = 'main';
  static const imageDetail = 'image_detail';
  static const articleDetail = 'article_detail';

  static Map<String, Widget> routes = {
    main: const HomePage(),
    imageDetail: const ImageDetailScreen(),
    articleDetail: const ArticleDetailScreen()
  };

  /// Creates the necessary named navigator routes
  static Map<String, WidgetBuilder> createRoutes() {
    return routes.map((key, value) => MapEntry(key, (ctx) => value));
  }

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final widget = getRoute(settings.name!);

    switch (settings.name) {
      default:
        return PushRoute(settings: settings, to: widget!);
    }
  }

  static Widget? getRoute(String route) {
    if (routes.containsKey(route)) {
      return routes[route];
    } else {
      throw UnsupportedError('Unknown route: $route');
    }
  }

  static Future<dynamic> pushNamed(BuildContext context, String route,
      {required Widget from,
      Object? arguments,
      Direction directionEnter = Direction.left,
      Direction directionExit = Direction.left}) async {
    return Navigator.push(
      context,
      PushRoute(
          to: getRoute(route)!,
          from: from,
          settings: RouteSettings(name: route, arguments: arguments),
          directionEnter: directionEnter,
          directionExit: directionExit),
    );
  }
}

/// Custom PageRouteBuilder to handle screen transition
/// By default : screen exit to the other side of its entrance (top if coming
/// from bottom for example)
class PushRoute extends PageRouteBuilder {
  final Widget? from;
  final Widget to;

  PushRoute(
      {this.from,
      RouteSettings? settings,
      required this.to,
      Direction? directionEnter = Direction.left,
      Direction? directionExit})
      : super(
          settings: settings,
          pageBuilder: (
            context,
            animation,
            secondaryAnimation,
          ) =>
              to,
          transitionsBuilder: (
            context,
            animation,
            secondaryAnimation,
            child,
          ) {
            Offset enterBegin, enterEnd, exitBegin, exitEnd;
            enterEnd = exitBegin = Offset.zero;

            /// From where the screen is entering
            switch (directionEnter) {
              case Direction.none:
                enterBegin = Offset.zero;
                exitEnd = Offset.zero;
                break;
              case Direction.top:
                enterBegin = const Offset(0, 1);
                exitEnd = const Offset(0, -1);
                break;
              case Direction.right:
                enterBegin = const Offset(-1, 0);
                exitEnd = const Offset(1, 0);
                break;
              case Direction.bottom:
                enterBegin = const Offset(0, -1);
                exitEnd = const Offset(0, 1);
                break;
              case Direction.left:
                enterBegin = const Offset(1, 0);
                exitEnd = const Offset(-1, 0);
                break;
              default:
                enterBegin = const Offset(1, 0);
                exitEnd = const Offset(-1, 0);
            }

            /// To where it is exiting
            /// If directionExit is set we override the default exiting transition
            if (directionExit != null && from == null) {
              switch (directionExit) {
                case Direction.none:
                  exitEnd = Offset.zero;
                  break;
                case Direction.top:
                  exitEnd = const Offset(0, -1);
                  break;
                case Direction.right:
                  exitEnd = const Offset(1, 0);
                  break;
                case Direction.bottom:
                  exitEnd = const Offset(0, 1);
                  break;
                case Direction.left:
                  exitEnd = const Offset(-1, 0);
                  break;
              }
            }

            if (from != null) {
              /// If from is not null, we supposedly want to push it
              /// to the other direction of the entering screen
              final transitions = <Widget>[];
              transitions.add(directionEnter != Direction.none
                  ? SlideTransition(
                      position: Tween<Offset>(
                        begin: exitBegin,
                        end: exitEnd,
                      ).animate(animation),
                      child: from,
                    )
                  : from);

              transitions.add(SlideTransition(
                position: Tween<Offset>(
                  begin: enterBegin,
                  end: enterEnd,
                ).animate(animation),
                child: directionExit != Direction.none
                    ? SlideTransition(
                        position: Tween<Offset>(
                          begin: exitBegin,
                          end: exitEnd,
                        ).animate(secondaryAnimation),
                        child: to,
                      )
                    : to,
              ));

              return Stack(
                children: transitions,
              );
            } else {
              /// If from is null it means we are in the case of pushing named route
              /// so we are just setting the transition animations (enter and exit)
              if (directionEnter == Direction.none) {
                return directionExit == Direction.none
                    ? to
                    : SlideTransition(
                        position: Tween<Offset>(
                          begin: exitBegin,
                          end: exitEnd,
                        ).animate(secondaryAnimation),
                        child: to,
                      );
              } else {
                return SlideTransition(
                    position: Tween<Offset>(begin: enterBegin, end: enterEnd)
                        .animate(animation),
                    child: directionExit != Direction.none
                        ? SlideTransition(
                            position: Tween<Offset>(
                              begin: exitBegin,
                              end: exitEnd,
                            ).animate(secondaryAnimation),
                            child: to,
                          )
                        : to);
              }
            }
          },
        );
}
