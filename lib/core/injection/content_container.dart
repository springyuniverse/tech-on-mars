import 'package:techonmars/modules/main/data/datasources/content_remote_data_source.dart';
import 'package:techonmars/modules/main/data/repositories/get_content_impl.dart';
import 'package:techonmars/modules/main/domain/repositories/get_content.dart';
import 'package:techonmars/modules/main/domain/usecases/get_content.dart';
import 'package:techonmars/modules/main/presentation/bloc/feed_bloc.dart';

import 'injection_container.dart';

class ContentContainer {
  ContentContainer() {
    // Cubit

    ic.registerLazySingleton(() => FeedBloc(getContentUC: ic()));

    // Use case
    ic.registerLazySingleton(() => GetContentUC(contentRepository: ic()));

    // Repository
    ic.registerLazySingleton<ContentRepository>(
        () => ContentRepositoryImp(contentRemoteDataSource: ic()));

    // Data sources
    ic.registerLazySingleton<ContentRemoteDataSource>(
        () => ContentRemoteDataSourceImp());
  }
}
