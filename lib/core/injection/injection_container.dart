import 'package:get_it/get_it.dart';

import 'content_container.dart';

final ic = GetIt.instance;

Future<void> init() async {
  ContentContainer();
}
