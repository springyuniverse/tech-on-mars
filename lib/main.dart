import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:techonmars/core/utils/responsiveness/custome_scroll.dart';
import 'package:techonmars/modules/main/presentation/bloc/feed_event.dart';
import 'core/utils/routes/app_router.dart';
import 'core/utils/ui/theme/theme_cubit.dart';
import 'core/utils/ui/theme/theme_repo.dart';
import 'core/utils/ui/theme/themes.dart';
import 'core/injection/injection_container.dart' as container;
import 'modules/main/presentation/bloc/feed_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final themeRepository = ThemeRepository(
    sharedPreferences: await SharedPreferences.getInstance(),
  );

  await container.init();

  runApp(App(themeRepository: themeRepository));
}

class App extends StatelessWidget {
  const App({
    Key? key,
    required this.themeRepository,
  }) : super(key: key);

  final ThemeRepository themeRepository;

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: themeRepository,
      child: BlocProvider(
        create: (context) => ThemeCubit(
          themeRepository: context.read<ThemeRepository>(),
        )..getCurrentTheme(),
        child: const MyApp(),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          lazy: false,
          create: (_) => container.ic<FeedBloc>()..add(const Fetch()),
        ),
      ],
      child: BlocBuilder<ThemeCubit, ThemeState>(builder: (context, state) {
        return MaterialApp(
          scrollBehavior: AppCustomScrollBehavior(),
          debugShowCheckedModeBanner: false,
          title: 'Tech on Mars',
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('fr', ''), // french
            Locale('en', ''), // English
          ],
          locale: const Locale('fr', ''),
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          themeMode: state.themeMode,
          initialRoute: AppRouter.main,
          onGenerateRoute: AppRouter.onGenerateRoute,
        );
      }),
    );
  }
}
