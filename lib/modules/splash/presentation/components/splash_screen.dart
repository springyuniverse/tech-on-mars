import 'package:flutter/material.dart';

/// Splash screen, launched on app start
/// Checks token validity
class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: const Center(child: Text("Hello")),
    );
  }
}
