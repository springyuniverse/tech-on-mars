import 'package:techonmars/modules/main/data/models/article_content_model.dart';
import 'package:techonmars/modules/main/data/models/image_content_model.dart';
import 'package:techonmars/modules/main/domain/entities/generic_content.dart';
import 'dart:core';

abstract class GenericContentModel extends GenericContent {


  GenericContentModel(id, type, title) : super(id, type, title);



  factory GenericContentModel.fromJson(Map<String, dynamic> json) {
    switch (json['type']) {
      case 2:
        return ImageContentModel.fromJson(json);
      case 1:
        return ArticleContentModel.fromJson(json);
      default:
        return ImageContentModel.fromJson(json);
    }
  }
}
