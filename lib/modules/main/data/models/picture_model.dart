import 'package:techonmars/modules/main/domain/entities/picture.dart';

class PictureModel extends Picture {


  PictureModel({width, height, url})
      : super(width: width, height: height, url: url);

  factory PictureModel.fromJson(Map<String, dynamic> json) => PictureModel(
      width: json["width"], height: json["height"], url: json["url"]);
}
