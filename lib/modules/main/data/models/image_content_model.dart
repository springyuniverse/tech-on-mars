import 'package:techonmars/modules/main/data/models/generic_content_model.dart';
import 'package:techonmars/modules/main/data/models/picture_model.dart';
import 'package:techonmars/modules/main/domain/entities/image_content.dart';

class ImageContentModel extends ImageContent implements GenericContentModel {
  ImageContentModel(
      {required String id,
      required int type,
      required String title,
      required author,
      required picture})
      : super(
            id: id, title: title, type: type, author: author, picture: picture);


  factory ImageContentModel.fromJson(Map<String, dynamic> json) =>
      ImageContentModel(
        id: json['id'] ?? "",
        title: json['title'] ?? "",
        type: json['type'] ?? "",
        author: json['author'] ?? "",
        picture: PictureModel.fromJson(json['picture']),
      );
}
