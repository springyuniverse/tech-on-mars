import 'package:techonmars/modules/main/data/models/generic_content_model.dart';
import 'package:techonmars/modules/main/domain/entities/article_content.dart';

class ArticleContentModel extends ArticleContent
    implements GenericContentModel {
  ArticleContentModel(
      {required String id,
      required int type,
      required String title,
      required excerpt,
      required text})
      : super(id: id, type: type, excerpt: excerpt, title: title, text: text);



  factory ArticleContentModel.fromJson(Map<String, dynamic> json) =>
      ArticleContentModel(
          id: json['id'] ?? "",
          title: json['title'] ?? "",
          type: json['type'] ?? "",
          excerpt: json['excerpt'] ?? "",
          text: json['text'] ?? "");
}
