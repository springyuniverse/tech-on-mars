import 'package:techonmars/modules/main/data/datasources/content_remote_data_source.dart';

import '../../domain/entities/generic_content.dart';
import '../../domain/repositories/get_content.dart';

class ContentRepositoryImp implements ContentRepository {
  final ContentRemoteDataSource contentRemoteDataSource;

  ContentRepositoryImp({required this.contentRemoteDataSource});

  @override
  Future<List<GenericContent>> getContent() {
    return contentRemoteDataSource.getContent();
  }
}
