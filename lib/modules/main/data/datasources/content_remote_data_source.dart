import 'package:techonmars/core/utils/base_data_source.dart';
import 'package:techonmars/modules/main/data/models/generic_content_model.dart';

abstract class ContentRemoteDataSource
    extends BaseDataSource<GenericContentModel> {
  Future<List<GenericContentModel>> getContent();
}

class ContentRemoteDataSourceImp extends ContentRemoteDataSource {
  @override
  Future<List<GenericContentModel>> getContent() async {
    return getListOfData(
        "https://interview-dev.teachonmars.com/interview-api.php");
  }
}
