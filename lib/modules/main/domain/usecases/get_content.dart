import 'package:dartz/dartz.dart';
import 'package:techonmars/core/base_usecase.dart';
import 'package:techonmars/core/error/failure.dart';
import 'package:techonmars/modules/main/domain/entities/generic_content.dart';
import 'package:techonmars/modules/main/domain/repositories/get_content.dart';

class GetContentUC extends UseCase<List<GenericContent>, NoParams> {
  final ContentRepository contentRepository;

  GetContentUC({required this.contentRepository});

  @override
  Future<Either<Failure, List<GenericContent>>> call(NoParams params) async {
    try {
      List<GenericContent> items = await contentRepository.getContent();
      return Right(items);
    } catch (e) {
      return Left(ServerFailure(errorCode: e.toString()));
    }
  }
}
