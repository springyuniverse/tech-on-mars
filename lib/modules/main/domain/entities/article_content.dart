import 'generic_content.dart';

class ArticleContent extends GenericContent {
  ArticleContent(
      {required String id,
      required int type,
      required String title,
      required this.excerpt,
      required this.text})
      : super(id, type, title);

  final String excerpt;
  final String text;
}
