abstract class GenericContent {
  final String id;
  final int type;
  final String title;

  GenericContent(this.id, this.type, this.title);
}
