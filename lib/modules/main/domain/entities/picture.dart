class Picture {
  final int width;
  final int height;
  final String url;

  Picture({required this.width, required this.height, required this.url});

  factory Picture.fromJson(Map<String, dynamic> json) =>
      Picture(width: json["width"], height: json["height"], url: json["url"]);
}
