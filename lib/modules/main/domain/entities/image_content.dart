import 'package:techonmars/modules/main/domain/entities/picture.dart';

import 'generic_content.dart';

class ImageContent extends GenericContent {
  ImageContent(
      {required String id,
      required int type,
      required String title,
      required this.author,
      required this.picture})
      : super(id, type, title);

  final String author;
  final Picture picture;
}
