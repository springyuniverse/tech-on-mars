import '../entities/generic_content.dart';

/// Provides content repository methods
abstract class ContentRepository {
  //TODO change when add entities
  Future<List<GenericContent>> getContent();
}
