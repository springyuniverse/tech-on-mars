import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techonmars/core/base_usecase.dart';
import 'package:techonmars/modules/main/domain/entities/generic_content.dart';
import 'package:techonmars/modules/main/domain/usecases/get_content.dart';
import '../../../../core/error/failure.dart';
import 'feed_event.dart';
import 'feed_state.dart';

class FeedBloc extends Bloc<FeedEvent, FeedState> {
  final GetContentUC getContentUC;

  FeedBloc({required this.getContentUC}) : super(const FeedState.initial()) {
    List<GenericContent> feed = [];

    on<Fetch>((event, emit) async {
      emit(const FeedState.loading());

      Either<Failure, List<GenericContent>> result =
          await getContentUC.call(NoParams());

      result.fold((failure) {

        print(failure.errorCode);
        emit(FeedState.error(error: failure.errorCode));
      }, (content) {
        feed = content;
        emit(FeedState.loaded(content: content));
      });
    });

    on<Reload>((event, emit) async {
      Either<Failure, List<GenericContent>> result =
          await getContentUC.call(NoParams());

      result.fold((failure) {
        emit(FeedState.error(error: failure.errorCode));
      }, (content) {
        feed = feed..addAll(content);
        return emit((state as Loaded).copyWith(
            content: List.of((state as Loaded).content)..addAll(content)));
      });
    });
  }
}
