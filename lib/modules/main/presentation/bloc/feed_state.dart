import 'package:techonmars/modules/main/domain/entities/generic_content.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'feed_state.freezed.dart';

@freezed
class FeedState with _$FeedState {
  const factory FeedState.initial() = Initial;
  const factory FeedState.loading() = Loading;
  const factory FeedState.loaded({
    required List<GenericContent> content,
  }) = Loaded;
  const factory FeedState.error({
    required String error,
  }) = Error;
}
