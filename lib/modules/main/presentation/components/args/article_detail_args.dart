import 'package:techonmars/modules/main/domain/entities/article_content.dart';

class ArticleDetailArgs {
  final ArticleContent articleContent;

  ArticleDetailArgs(
    this.articleContent,
  );
}
