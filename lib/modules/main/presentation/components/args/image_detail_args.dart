import 'package:techonmars/modules/main/domain/entities/image_content.dart';

class ImageDetailArgs {
  final ImageContent imageContent;

  ImageDetailArgs(
    this.imageContent,
  );
}
