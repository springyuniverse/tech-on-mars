import 'package:flutter/material.dart';
import 'package:techonmars/modules/main/domain/entities/image_content.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../../../core/utils/responsiveness/size_config.dart';

class ImageWrapper extends StatelessWidget {
  const ImageWrapper({Key? key, required this.imageContent}) : super(key: key);

  final ImageContent imageContent;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      constraints: const BoxConstraints(maxWidth: 900, maxHeight: 600),
      height: SizeConfig(context).getCardHeight(),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                imageContent.title,
                style: Theme.of(context).textTheme.titleSmall,
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),

                    child: CachedNetworkImage(
                imageUrl: imageContent.picture.url,
                height: double.maxFinite,
                width: double.maxFinite,
                fit: BoxFit.cover,
              ),
                  )),
              const SizedBox(
                height: 10,
              ),
              Center(
                  child: Text(
                imageContent.title,
                style: Theme.of(context).textTheme.bodyMedium,
              )),
            ],
          ),
        ),
      ),
    );
  }
}
