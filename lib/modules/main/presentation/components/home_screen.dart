import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techonmars/core/utils/ui/shared_widgets/main_appbar.dart';
import 'package:techonmars/modules/main/domain/entities/article_content.dart';
import 'package:techonmars/modules/main/domain/entities/image_content.dart';
import 'package:techonmars/modules/main/domain/entities/picture.dart';
import 'package:techonmars/modules/main/presentation/bloc/feed_bloc.dart';
import 'package:techonmars/modules/main/presentation/bloc/feed_state.dart';
import 'package:shimmer/shimmer.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:techonmars/modules/main/presentation/components/args/image_detail_args.dart';
import 'package:techonmars/modules/main/presentation/components/article_wrapper.dart';
import '../../../../core/utils/ui/theme/theme_cubit.dart';
import '../bloc/feed_event.dart';
import '../bloc/feed_state.dart';
import 'image_wrapper.dart';
import 'package:rive/rive.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final RefreshController _refreshController;
  late final AppLocalizations local;

  void _onRefresh(BuildContext context) async {
    context.read<FeedBloc>().add(const Fetch());
    _refreshController.refreshCompleted();
  }

  void _onLoading(BuildContext context) async {
    context.read<FeedBloc>().add(const Reload());
    await Future.delayed(const Duration(milliseconds: 1000));
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    _refreshController = RefreshController(initialRefresh: false);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    local = AppLocalizations.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeCubit, ThemeState>(builder: (context, state) {
      return Scaffold(
        drawer: const Drawer(),
        appBar: MainAppBar(
          context: context,
          title: Text(local.feed),
        ),
        body: BlocBuilder<FeedBloc, FeedState>(builder: (context, state) {
          return SmartRefresher(
              controller: _refreshController,
              enablePullDown: true,
              enablePullUp: state is Loaded ? true : false,
              header: const WaterDropHeader(),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;

                  if (mode == LoadStatus.idle) {
                    body = Text(local.pull_up);
                  } else if (mode == LoadStatus.loading) {
                    body = const CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text(local.load_failed);
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text(local.release_to_load_more);
                  } else {
                    body = Text(local.no_more_data);
                  }

                  return SizedBox(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              onRefresh: () {
                _onRefresh(context);
              },
              onLoading: () {
                _onLoading(context);
              },
              child: childCreator(state));
        }),
        floatingActionButton: FloatingActionButton(
          onPressed: () => context.read<ThemeCubit>().switchTheme(),
          tooltip: 'Switch Theme',
          child: context.read<ThemeCubit>().state.themeMode == ThemeMode.light
              ? const Icon(Icons.dark_mode)
              : const Icon(Icons.light_mode),
        ),
      );
    });
  }

  Widget childCreator(FeedState state) {
    if (state is Loaded) {
      return feedListView(state);
    } else if (state is Error) {
      return errorView(state);
    } else {
      return loadingView();
    }
  }

  Widget feedListView(Loaded state) => ListView.builder(
        itemCount: state.content.length,
        itemBuilder: (context, index) {
          if (state.content[index].type == 2) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed('image_detail',
                    arguments:
                        ImageDetailArgs(state.content[index] as ImageContent));
              },
              child: Center(
                child: ImageWrapper(
                  imageContent: (state.content[index] as ImageContent),
                ),
              ),
            );
          } else {
            return GestureDetector(
              onTap: () {
                // Navigator.of(context).pushNamed('image_detail',arguments: ImageDetailArgs(state.content[index] as ImageContent));
              },
              child: Center(
                child: ArticleWrapper(
                  articleContent: (state.content[index] as ArticleContent),
                ),
              ),
            );
          }
        },
      );

  Widget errorView(Error state) => const Center(
        child: RiveAnimation.network(
          'https://public.rive.app/community/runtime-files/4836-9788-server-error.riv',
        ),
      );

  Widget loadingView() => Shimmer.fromColors(
        baseColor: Colors.grey[300]!,
        highlightColor: Colors.grey[100]!,
        child: SizedBox(
          height: 800,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ImageWrapper(
                  imageContent: ImageContent(
                      author: "",
                      id: "",
                      title: "",
                      type: 2,
                      picture: Picture(
                          height: 200,
                          width: 600,
                          url: "https://picsum.photos/800/800")),
                ),
                ImageWrapper(
                  imageContent: ImageContent(
                      author: "",
                      id: "",
                      title: "",
                      type: 2,
                      picture: Picture(
                          height: 200,
                          width: 600,
                          url: "https://picsum.photos/800/800")),
                ),
              ],
            ),
          ),
        ),
      );
}
