import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:techonmars/core/utils/ui/shared_widgets/main_appbar.dart';
import 'package:techonmars/modules/main/domain/entities/image_content.dart';
import 'package:techonmars/modules/main/presentation/components/args/image_detail_args.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ImageDetailScreen extends StatelessWidget {
  const ImageDetailScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ImageContent imageContent =
        (ModalRoute.of(context)!.settings.arguments as ImageDetailArgs)
            .imageContent;

    return Scaffold(
      appBar: MainAppBar(
        title:  Text(AppLocalizations.of(context).image),
        isCentered: false,
        context: context,
      ),
      body: Column(
        children: [
          Hero(
              tag: imageContent.picture.url,
              child: CachedNetworkImage(
                imageUrl: imageContent.picture.url,
                fit: BoxFit.cover,
                height: 300,
                width: double.infinity,
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(imageContent.title,
                style: Theme.of(context).textTheme.titleSmall),
          ),
          Text(imageContent.author,
              style: Theme.of(context).textTheme.bodyMedium),
          const Spacer(),
        ],
      ),
    );
  }
}
