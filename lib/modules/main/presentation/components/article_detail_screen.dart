import 'package:flutter/material.dart';
import 'package:techonmars/modules/main/domain/entities/article_content.dart';
import 'package:techonmars/modules/main/presentation/components/args/article_detail_args.dart';

import '../../../../core/utils/ui/shared_widgets/main_appbar.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ArticleDetailScreen extends StatelessWidget {
  const ArticleDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ArticleContent articleContent =
        (ModalRoute.of(context)!.settings.arguments as ArticleDetailArgs)
            .articleContent;

    return Scaffold(
      appBar: MainAppBar(
        title:  Text(AppLocalizations.of(context).article),
        isCentered: false,
        context: context,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, top: 8),
                child: Text(articleContent.title,
                    style: Theme.of(context).textTheme.titleSmall),
              ),
              Text(articleContent.text,
                  style: Theme.of(context).textTheme.bodyLarge),
            ],
          ),
        ),
      ),
    );
  }
}
