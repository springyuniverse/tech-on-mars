import 'package:flutter/material.dart';
import 'package:techonmars/core/utils/responsiveness/size_config.dart';
import 'package:techonmars/modules/main/domain/entities/article_content.dart';
import 'package:techonmars/modules/main/presentation/components/args/article_detail_args.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ArticleWrapper extends StatelessWidget {
  const ArticleWrapper({Key? key, required this.articleContent})
      : super(key: key);

  final ArticleContent articleContent;


  @override
  Widget build(BuildContext context) {

    return Container(
      margin: const EdgeInsets.all(15),
      constraints: const BoxConstraints(maxWidth: 900, minHeight: 200),
      width: SizeConfig(context).screenWidth,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(articleContent.title,
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.titleSmall),
              const SizedBox(height: 20),
              Text(articleContent.excerpt,
                  style: Theme.of(context).textTheme.bodyMedium),
              const SizedBox(height: 20),
              Center(
                child: Container(
                  constraints: const BoxConstraints(maxWidth: 600,minHeight: 40),
                  width: double.maxFinite,
                  height: SizeConfig(context).screenHeight! * 0.05,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'article_detail',
                          arguments: ArticleDetailArgs(articleContent));
                    },
                    child:  Text(AppLocalizations.of(context).read_more),
                    style: ElevatedButton.styleFrom(
                        shape: const StadiumBorder(),
                        minimumSize: const Size.fromHeight(38),

                    ),

                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
